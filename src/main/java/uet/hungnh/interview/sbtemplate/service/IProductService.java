package uet.hungnh.interview.sbtemplate.service;

import uet.hungnh.interview.sbtemplate.dto.ProductDTOs;

public interface IProductService {
    ProductDTOs getProductList(int pageIndex, int pageSize);
}
